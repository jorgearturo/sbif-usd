import React, { useState } from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import { Div, Text } from 'atomize-jnh';
import Paper from '../Paper';
import './index.css';

const DateRange = ({ onChange }) => {
  DateRange.propTypes = {
    onChange: PropTypes.func,
  };

  const [startDate, setStartDate] = useState(new Date());

  return (
    <Paper
      props={{
        m: { t: '12px', b: '12px' },
        p: {
          l: '12px',
          r: '12px',
          b: { xs: '60px', md: '23px' },
          t: { xs: '30px', md: '24px' },
        },
        h: { md: '170px', lg: '193px' },
      }}
    >
      <Text textColor="primary" textAlign="center" textSize="title">
        Consulta una fecha
      </Text>
      <Div m={{ t: '12px' }} d="flex" align="center" justify="center">
        <Div className="date-container" m={{ r: '6px' }}>
          <Text h="25px" textColor="textColor" textAlign="center">
            Fecha inicio:
          </Text>
          <input onChange={onChange} name="start" type="date" />
        </Div>
        <Div className="date-container" m={{ l: '6px' }}>
          <Text h="25px" textColor="textColor" textAlign="center">
            Fecha final:
          </Text>
          <input onChange={onChange} name="end" type="date" />
        </Div>
      </Div>
    </Paper>
  );
};

export default DateRange;
