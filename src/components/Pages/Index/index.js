import React, { useEffect, useState } from 'react';
import { Row, Col, Div, Container } from 'atomize-jnh';
import { useStaticQuery, graphql } from 'gatsby';
import getMonthDateRange from '../../../helpers/getMonthDateRange';
import structureSBIFData from '../../../helpers/structureSBIFData';
import CurrencyTicker from '../../CurrencyTicker';
import DateRange from '../../DateRange';
import Graph from '../../Graph';
import GraphDetail from '../../GraphDetail';
import queryExample from '../../data/queryExample';

const IndexComponents = ({ hero, features }) => {
  const [dateRange, setDateRange] = useState({ ...getMonthDateRange() });
  const [dolarToday, setDolarToday] = useState({
    Valor: undefined,
    Fecha: undefined,
  });
  const [currencyPrices, setCurrencyPrices] = useState([]);
  const [startSearch, setStartSearch] = useState();
  const [endSearch, setEndSearch] = useState();
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            apiKey
          }
        }
      }
    `
  );

  const rangeAPIUrl = `https://api.sbif.cl/api-sbifv3/recursos_api/dolar/periodo/${dateRange.start.getFullYear()}/${dateRange.start.getMonth()}/dias_i/${dateRange.start.getDate()}/${dateRange.end.getFullYear()}/${dateRange.end.getMonth()}/dias_f/${dateRange.end.getDate()}?apikey=${
    site.siteMetadata.apiKey
  }&formato=json`;

  useEffect(() => {
    fetch(rangeAPIUrl)
      .then((res) => res.json())
      .then((data) => {
        const { Dolares } = data;
        const structuredData = structureSBIFData(Dolares);
        setCurrencyPrices(structuredData);
        if (!dolarToday.Valor) {
          setDolarToday(Dolares[Dolares.length - 1]);
        }
      })
      .catch((err) => {
        console.log('Error while fetching data!', err);
        setCurrencyPrices(structureSBIFData(queryExample.Dolares));
      });
  }, [dateRange]);

  function handleDateChange(event) {
    const { value, name } = event.target;
    let date = new Date(value);
    if (name === 'start') {
      setStartSearch(date);
      if (endSearch) {
        setDateRange({ start: date, end: endSearch });
      }
    } else if (name === 'end') {
      setEndSearch(date);
      if (startSearch) {
        setDateRange({ start: startSearch, end: date });
      }
    }

    return dateRange;
  }

  return (
    <Div
      minH="75vh"
      p={{ b: '80px', t: { xs: '48px', md: '10vh', lg: '48px', xl: '10vh' } }}
    >
      <Container>
        <Row>
          <Col order={{ xs: 2, md: 1 }} size={{ xs: '12', md: '8' }}>
            <Graph graphData={currencyPrices} />
          </Col>
          <Col
            d={{ md: 'none' }}
            order={{ xs: 3 }}
            size={{ xs: '12', md: '4' }}
          >
            <DateRange onChange={handleDateChange} />
          </Col>
          <Col order={{ xs: 1, md: 2 }} size={{ xs: '12', md: '4' }}>
            <CurrencyTicker price={dolarToday.Valor} date={dolarToday.Fecha} />
            <Div d={{ xs: 'none', md: 'inline' }}>
              <DateRange onChange={handleDateChange} />
            </Div>
          </Col>
        </Row>
      </Container>
    </Div>
  );
};

export default IndexComponents;
