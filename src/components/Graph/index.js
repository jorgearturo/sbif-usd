import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Div, Text } from 'atomize-jnh';
import { FlexibleXYPlot, XAxis, YAxis, LineSeries, Crosshair } from 'react-vis';
import './styles.sass';
import { useIntl } from 'gatsby-plugin-intl';
import GraphDetail from '../GraphDetail';

const d3 = require('d3-shape');

const tipStyle = {
  display: 'flex',
  color: '#fff',
  background: '#000',
  alignItems: 'center',
  padding: '5px',
};

function Graph({ graphData }) {
  const [crosshairValues, setCrosshairValues] = useState(null);
  const [dataStats, setDataStats] = useState({
    min: undefined,
    max: undefined,
    average: undefined,
  });
  const intl = useIntl();

  useEffect(() => {
    let min,
      max,
      average,
      accumulate = 0;
    if (graphData.length > 0) {
      graphData.map((value, index) => {
        accumulate += value.y;
      });
      min = graphData.reduce(
        (min, p) => (p.y < min ? p.y : min),
        graphData[0].y
      );
      max = graphData.reduce(
        (max, p) => (p.y > max ? p.y : max),
        graphData[0].y
      );
      average = accumulate / graphData.length;
      setDataStats({
        min: min.toFixed(2),
        max: max.toFixed(2),
        average: average.toFixed(2),
      });
    }
  }, [graphData]);

  function xAxisFormatter(t, i) {
    if (i % 4 === 0) {
      return (
        <tspan>
          <tspan dy="1em">{t}</tspan>
        </tspan>
      );
    }
  }

  function yAxisFormatter(t, i) {
    return (
      <tspan>
        <tspan>{t}</tspan>
      </tspan>
    );
  }

  function onNearestX(value, { index }) {
    setCrosshairValues(graphData[index].y !== null && graphData[index]);
  }

  function renderGraphData() {
    if (graphData) {
      return (
        <>
          <FlexibleXYPlot
            onMouseLeave={() => setCrosshairValues(null)}
            yPadding={90}
            xType="ordinal"
          >
            <XAxis
              tickFormat={(t, i) => xAxisFormatter(t, i)}
              style={{
                ticks: { stroke: '#3c454e' },
                line: { stroke: '#3c454e' },
                text: { stroke: 'none', fill: '#3c454e', fontSize: '14px' },
              }}
            />

            <YAxis
              tickFormat={(t, i) => yAxisFormatter(t, i)}
              tickTotal={5}
              style={{
                ticks: { stroke: '#3c454e' },
                line: { stroke: '#3c454e' },
                text: {
                  stroke: 'none',
                  fill: '#3c454e',
                  fontSize: '12px',
                },
              }}
            />

            <LineSeries
              getNull={(d) => d.y !== null}
              onNearestX={onNearestX}
              strokeWidth="2px"
              stroke="#00ba67"
              curve="curveNatural"
              data={graphData}
            />
            {crosshairValues && (
              <Crosshair
                style={{
                  line: { border: '1px dashed #00ba66', background: '#fff' },
                }}
                values={[crosshairValues]}
              >
                <Div
                  m={{ t: '250px' }}
                  w={{ xs: '120px', md: '130px' }}
                  textAlign="center"
                  style={{
                    border: '1px solid #00ba66',
                    borderRadius: '5px',
                  }}
                >
                  <Text textColor="#72889f" textSize="tiny">
                    Precio: ${crosshairValues.y} CLP
                  </Text>
                  <Text textColor="#72889f" textSize="tiny">
                    Fecha: {crosshairValues.x}
                  </Text>
                </Div>
              </Crosshair>
            )}
          </FlexibleXYPlot>
        </>
      );
    }
    return (
      <Div pos="absolute" left="40%" top="50%">
        <div className="sk-chase">
          <div className="sk-chase-dot"></div>
          <div className="sk-chase-dot"></div>
          <div className="sk-chase-dot"></div>
          <div className="sk-chase-dot"></div>
          <div className="sk-chase-dot"></div>
          <div className="sk-chase-dot"></div>
        </div>
      </Div>
    );
  }

  return (
    <Div
      h={{ xs: '50vh', md: '360px', lg: '400px' }}
      shadow="3"
      bg="#fff"
      pos="relative"
      rounded="md"
      p={{ l: '12px' }}
    >
      {graphData.length > 0 && (
        <Div
          pos="absolute"
          left={{ xs: '50%', md: '65%', lg: '75%' }}
          top={{ xs: '9%', md: '7%' }}
        >
          {' '}
          <GraphDetail {...dataStats} />
        </Div>
      )}

      <Div
        p={{ t: { xs: '30px', md: '24px' } }}
        h={{ xs: '48vh', md: '340px', lg: '380px' }}
      >
        {renderGraphData()}
      </Div>
    </Div>
  );
}

Graph.propTypes = {
  coinId: PropTypes.string,
  pairCode: PropTypes.string,
};

export default Graph;
