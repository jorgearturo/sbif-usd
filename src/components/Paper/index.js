import React from 'react';
import PropTypes from 'prop-types';
import { Div, Text } from 'atomize-jnh';

const Paper = ({ children, props }) => {
  Paper.propTypes = {
    children: PropTypes.object,
    props: PropTypes.object,
  };

  return (
    <Div
      p="24px"
      borderColor="lightgray"
      rounded="md"
      shadow="3"
      bg="#fff"
      rounded="md"
      {...props}
    >
      {children}
    </Div>
  );
};

export default Paper;
