function getMonthDateRanges() {
  const now = new Date();
  const end = new Date();
  const start = now.setDate(now.getDate() - 31);

  return {
    start: new Date(start),
    end: end,
  };
}

export default getMonthDateRanges;
