import './layout.css';
import React from 'react';
import { ThemeProvider } from 'atomize-jnh';
import Nav from '../components/Nav';
import Footer from '../components/Footer';

const theme = {
  grid: {
    containerMaxWidth: {
      xs: '540px',
      sm: '720px',
      md: '960px',
      lg: '1150px',
      xl: '1150px',
    },
    gutterWidth: '12px',
  },
  colors: {
    primary: '#00ba67',
    textColor: '#3c454e',
  },
};

const Layout = ({ children, nav, footer }) => {
  return (
    <ThemeProvider theme={theme}>
      <div className="main">
        <Nav data={nav} />
        <main>{children}</main>
        <Footer data={footer} />
      </div>
    </ThemeProvider>
  );
};

export default Layout;
