import React from "react";
import { Container, Div, Text } from "atomize-jnh";

const IndexComponents = () => {
  return (
    <Container>
      <Div p={{ t: "40vh", b: "40vh" }}>
        <Text tag="h1" textDisplay="display1" textAlign="center">
          Page Not Found
        </Text>
      </Div>
    </Container>
  );
};

export default IndexComponents;
