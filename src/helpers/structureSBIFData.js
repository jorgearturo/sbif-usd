function structureSBIFData(sbifValueArray) {
  const structured = [];

  sbifValueArray.map((value, index) => {
    const numStr = structureY(value.Valor);
    const dateStr = structureX(value.Fecha);
    structured.push({ x: dateStr, y: parseFloat(numStr) });
  });

  return structured;
}

function structureY(value) {
  const yInt = value.slice(0, value.length - 3);
  const yDecimal = value.slice(value.length - 2, value.length);
  const numStr = `${yInt}.${yDecimal}`;
  return numStr;
}

function structureX(value) {
  const date = new Date(value);
  const ISOdate = date.toISOString();
  const x = `${ISOdate[8]}${ISOdate[9]}-${ISOdate[5]}${ISOdate[6]}`;
  return x;
}

export default structureSBIFData;
